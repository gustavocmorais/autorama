#include <stdio.h>
#include <math.h>
#include "mgos.h"
#include "mgos_blynk.h"
#include "common/json_utils.h"

#include "mgos_app.h"
#include "mgos_gpio.h"
#include "mgos_timers.h"
#include "mgos_config.h"



//status do motor
int status_up = 0;
int status_down = 0; 
int old_switch_state = -1;
mgos_timer_id ptr_timer;

// as pulseIn isn't supported in Mongoose Arduino compatability library yet, here's a local
// implementation of that. Full credit to "nliviu" on Mongoose OS forums for that
// https://forum.mongoose-os.com/discussion/1928/arduino-compat-lib-implicit-declaration-of-function-pulsein#latest
static inline uint64_t uptime()
{
    return (uint64_t)(1000000 * mgos_uptime());
}

uint32_t pulseInLongLocal(uint8_t pin, uint8_t state, uint32_t timeout)
{
    uint64_t startMicros = uptime();

    // wait for any previous pulse to end
    while (state == mgos_gpio_read(pin))
    {
        if ((uptime() - startMicros) > timeout)
        {
            return 0;
        }
    }

    // wait for the pulse to start
    while (state != mgos_gpio_read(pin))
    {
        if ((uptime() - startMicros) > timeout)
        {
            return 0;
        }
    }

    uint64_t start = uptime();

    // wait for the pulse to stop
    while (state == mgos_gpio_read(pin))
    {
        if ((uptime() - startMicros) > timeout)
        {
            return 0;
        }
    }
    return (uint32_t)(uptime() - start);
}


static double read_distance() {
  
  //send trigger
  mgos_gpio_write(mgos_sys_config_get_app_gpio_trigger_pin(), 1);
  // wait 10 microseconds
  mgos_usleep(10);
  // stop the trigger
  mgos_gpio_write(mgos_sys_config_get_app_gpio_trigger_pin(), 0);

  // wait for response and calculate distance
  unsigned long duration = pulseInLongLocal(mgos_sys_config_get_app_gpio_echo_pin(), 1, mgos_sys_config_get_app_pulse_in_timeout_usecs());
  double distance = duration * 0.034 / 2;
  
  //LOG(LL_INFO, ("duration: %lu", duration));
  LOG(LL_INFO, ("distance: %f", distance));

  return distance;
}



 
static void net_cb(int ev, void *evd, void *arg) {
  switch (ev) {
    case MGOS_NET_EV_DISCONNECTED:
      LOG(LL_INFO, ("%s", "Net disconnected"));
      break;
    case MGOS_NET_EV_CONNECTING:
      LOG(LL_INFO, ("%s", "Net connecting..."));
      break;
    case MGOS_NET_EV_CONNECTED:
      LOG(LL_INFO, ("%s", "Net connected"));
      break;
    case MGOS_NET_EV_IP_ACQUIRED:
      LOG(LL_INFO, ("%s", "Net got IP address"));
      break;
  }
 
  (void) evd;
  (void) arg;
}
 

static void report_to_blynk() {
  
  
  void *c = mgos_get_blynk_connection();

  if (c) {
    LOG(LL_INFO, ("report_to_blynk: sendind data"));
    //blynk_virtual_write(c, 1, (float) mgos_get_free_heap_size() / 1024 , 0);
    //blynk_virtual_write(c, 3, read_distance(), 0 );
    int pow = mgos_sys_config_get_app_powered();
    
    if (pow == 0) {
      blynk_virtual_write(c, 8, 0, 0 );
      blynk_virtual_write(c, 9, 0, 0 );
    }
    //subindo
    else if (pow > 0) {
      blynk_virtual_write(c, 8, 1, 0 );
      blynk_virtual_write(c, 9, 0, 0 );
    }
    //descendo
    else if (pow < 0) {
      blynk_virtual_write(c, 8, 0, 0 );
      blynk_virtual_write(c, 9, 1, 0 );
    }
  }
  else {
    LOG(LL_INFO, ("report_to_blynk: blynk not connected"));
  }
 
}


static void report_status() {
  report_to_blynk();
}

static void set_status_up(int new_status) {
  mgos_gpio_set_mode(mgos_sys_config_get_app_relay_up(), MGOS_GPIO_MODE_OUTPUT);
  mgos_gpio_write(mgos_sys_config_get_app_relay_up(), !new_status);
  status_up = new_status;
}

static void set_status_down(int new_status) {
  mgos_gpio_set_mode(mgos_sys_config_get_app_relay_down(), MGOS_GPIO_MODE_OUTPUT);
  mgos_gpio_write(mgos_sys_config_get_app_relay_down(), !new_status);
  status_down = new_status;
}

static void set_powered(int val) {
  mgos_sys_config_set_app_powered(val);
  char *err = NULL;
  save_cfg(&mgos_sys_config, &err); 
  LOG(LL_INFO, ("Saving configuration: %s\n", err ? err : "no error"));
  free(err);
}


static void set_target_max(double val) {
  mgos_sys_config_set_app_target_max(val);
  char *err = NULL;
  save_cfg(&mgos_sys_config, &err); 
  LOG(LL_INFO, ("Saving configuration: %s\n", err ? err : "no error"));
  free(err);
}

static void set_target_min(double val) {
  mgos_sys_config_set_app_target_min(val);
  char *err = NULL;
  save_cfg(&mgos_sys_config, &err); 
  LOG(LL_INFO, ("Saving configuration: %s\n", err ? err : "no error"));
  free(err);
}


static void stop_all() {
  set_status_up(0);
  set_status_down(0);
  mgos_sys_config_set_app_powered(0);
}


static void check_status (void *arg) {
  
  float temp = read_distance();

  //LOG(LL_INFO, ("status powered: %d",mgos_sys_config_get_app_powered() ));

  if (mgos_sys_config_get_app_powered() != 0) {
  
    

    if (mgos_sys_config_get_app_powered() < 0 && temp < mgos_sys_config_get_app_target_min()) {
      LOG(LL_INFO, ("chegou na distancia minima desligando!"));
      stop_all();
    }

    else if (mgos_sys_config_get_app_powered() < 0 && temp >= mgos_sys_config_get_app_target_min()) {
      LOG(LL_INFO, ("ta abaixo do minimo. descendo..."));
      set_status_down(1);
    }
    

    else if (mgos_sys_config_get_app_powered() > 0 && temp <= mgos_sys_config_get_app_target_max()) {
      LOG(LL_INFO, ("ta abaixo do maximo. subindo..."));
      set_status_up(1);
    }

    else if (mgos_sys_config_get_app_powered() > 0 && temp > mgos_sys_config_get_app_target_max()) {
      LOG(LL_INFO, ("chegou na distancia maxima desligando!"));
      stop_all();
    }

    else {
      LOG(LL_INFO, ("sem acao..."));
    }

    //atualizando status dos botoes no blynk
    report_status();

  }

  else {
    stop_all();
  }

  (void) arg;
}




static void blynk_handler(struct mg_connection *c, const char *cmd,
                                  int pin, int val, int id, void *user_data) {
  
  LOG(LL_INFO, ("blynk_handler: cmd: %s pin:%d val:%d id:%d",cmd, pin, val, id));
  
  //READS
  if (strcmp(cmd, "vr") == 0) {
    
    //memoria livre
    if (pin == 1) {
      LOG(LL_INFO, ("blynk_handler: mem"));
      blynk_virtual_write(c, pin, (float) mgos_get_free_heap_size() / 1024 , id);
    }
    //temperatura
    else if (pin == 3) {
      LOG(LL_INFO, ("blynk_handler: dist"));
      blynk_virtual_write(c, pin, read_distance(), id );
    }

    else if (pin == 4) {
      LOG(LL_INFO, ("blynk_handler: status_up"));
      blynk_virtual_write(c, pin, status_up, id );
    }

    else if (pin == 5) {
      LOG(LL_INFO, ("blynk_handler: status_down"));
      blynk_virtual_write(c, pin, status_down, id );
    }

    //temp alvo
    else if (pin == 6) {
      LOG(LL_INFO, ("blynk_handler: target min"));
      blynk_virtual_write(c, pin, mgos_sys_config_get_app_target_min(), id );
    }

    else if (pin == 7) {
      LOG(LL_INFO, ("blynk_handler: target max"));
      blynk_virtual_write(c, pin, mgos_sys_config_get_app_target_max(), id );
    }

    
    else if (pin == 10) {
      LOG(LL_INFO, ("blynk_handler: powered"));
      blynk_virtual_write(c, pin, mgos_sys_config_get_app_powered(), id );
    }

  } 
  
  //WRITES
  else if (strcmp(cmd, "vw") == 0) {
    
    LOG(LL_INFO, ("blynk_handler. pin:%d", pin));

    //sobe
    if (pin == 8) {
      LOG(LL_INFO, ("blynk_handler: sobe"));
      set_powered(val);
      check_status(NULL);
    }

    //desce
    if (pin == 9) {
      LOG(LL_INFO, ("blynk_handler: desce"));
      set_powered(-1 * val); //invertendo sinal
      check_status(NULL);
    }

    //target temp
    else if (pin == 6) {
      LOG(LL_INFO, ("blynk_handler: target min"));
      set_target_min(val);
      check_status(NULL);
    }

    else if (pin == 7) {
      LOG(LL_INFO, ("blynk_handler: target max"));
      set_target_max(val);
      check_status(NULL);
    }

    //reset
    else if (pin == 100) {
      LOG(LL_INFO, ("blynk_handler: restart"));
      mgos_system_restart();
    }

  }
  
  (void) user_data;
}


enum mgos_app_init_result mgos_app_init(void) {

  stop_all();

  // set the modes for the pins
  mgos_gpio_set_mode(mgos_sys_config_get_app_gpio_trigger_pin(), MGOS_GPIO_MODE_OUTPUT);
  mgos_gpio_set_mode(mgos_sys_config_get_app_gpio_echo_pin(), MGOS_GPIO_MODE_INPUT);
  mgos_gpio_set_pull(mgos_sys_config_get_app_gpio_echo_pin(), MGOS_GPIO_PULL_UP);


  /* Check temp */
  mgos_set_timer(mgos_sys_config_get_app_sensor_read_interval_ms(), MGOS_TIMER_REPEAT, check_status, NULL);
 
  /* Network connectivity events */
  mgos_event_add_group_handler(MGOS_EVENT_GRP_NET, net_cb, NULL);

  blynk_set_handler(blynk_handler, NULL);

  //reboot every 12h
  mgos_system_restart_after(43200*1000);

  check_status(NULL);

  stop_all();

  return MGOS_APP_INIT_SUCCESS;
}


